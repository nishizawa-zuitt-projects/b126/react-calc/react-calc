import { useState } from 'react';
import { Container, Form, Col } from 'react-bootstrap';

export default function Home(){

	const [numOne, setNumOne] = useState(0);
	const [numTwo, setNumTwo] = useState(0);
	const [answer, setAnswer] = useState(0);

	const addNums = (e) => {
		e.preventDefault()
		setAnswer(parseInt(numOne)+parseInt(numTwo))
	}

	const subNums = (e) => {
		e.preventDefault()
		setAnswer(numOne - numTwo)
	}

	const multiNums = (e) => {
		e.preventDefault()
		setAnswer(numOne*numTwo)
	}

	const divNums = (e) => {
		e.preventDefault()
		setAnswer(numOne/numTwo)
	}

	const resetNums = (e) => {
		setNumOne(0);
		setNumTwo(0);
		setAnswer(0);
	}

	return(
		<Container>
			<div className="my-5">
				<h1 className="text-center">Calculator</h1>
			</div>

			<div className="my-5 text-center">
				<h2>{answer}</h2>
			</div>
			<Form className="my-5 text-center">
				<Form.Row className="my-5">
					<Form.Group as={Col} controlId="numOne">
						<Form.Control type="number" required value={numOne} onChange={e => setNumOne(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="numTwo">
						<Form.Control type="number" required value={numTwo} onChange={e => setNumTwo(e.target.value)}/>
					</Form.Group>
				</Form.Row>
				<div className="text-center">
					<button className="btn btn-primary" onClick={e=> addNums(e)} >Add</button>
					<button className="btn btn-primary mx-2" onClick={e=> subNums(e)}>Subtract</button>
					<button className="btn btn-primary" onClick={e=> multiNums(e)}>Multiply</button>
					<button className="btn btn-primary mx-2" onClick={e=> divNums(e)}>Divide</button>
					<button className="btn btn-primary" onClick={e=> resetNums(e)}>Reset</button>
				</div>
			</Form>
		</Container>
	)
}